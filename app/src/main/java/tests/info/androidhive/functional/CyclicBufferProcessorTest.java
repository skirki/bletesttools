package tests.info.androidhive.functional;

import android.test.InstrumentationTestCase;

import info.androidhive.functional.CyclicBufferProcessor;
import tests.info.androidhive.functional.mocks.TapCollectorMock;

/**
 * Created by hjacker on 17.05.15.
 */
public class CyclicBufferProcessorTest extends InstrumentationTestCase{

        public void test() throws Exception {

            final int expected = 5;
            final int reality = 5;

            assertEquals(expected, reality);

        }

    public void testOneIncomingFrame(){
        /*
            The frame starts with 0xff <sign> then has <length> value
                in the middle there is -length byte values
            Frame end with <CRC> which is 0xff-SUM(values..) and <CR><LF>
         */
        /*                    <sign>,<length>,  <..values[length].. >, <CRC>=0xff-SUM(values),<CR>,       <LF> */
        byte [] frame = { (byte)0xff,(byte)0x02,(byte)0x01,(byte)0x02,(byte)0xfc,             (byte)0x0d, (byte)0x0a };


        //Given
        byte[] inputOneFrame = frame;
        CyclicBufferProcessor processor = new CyclicBufferProcessor(null);

        //When
        processor.processInput(inputOneFrame, frame.length);

        //Then
        assertEquals(processor.stackSize(), 1);
        byte [] f = processor.pop();
        assertEquals(frame[1]+2, f.length);
        assertEquals(frame[2],   f[2]);
        assertEquals(frame[3],   f[3]);

    }

    public void test_CRC_SumBiggerThan_255(){
        /*
            The frame starts with 0xff <sign> then has <length> value
                in the middle there is -length byte values
            Frame end with <CRC> which is SUM(values..)%255 and <CR><LF>
         */
        /* <sign>,<length>,  <..values[length].. >, <CRC>=0xff-SUM(values),<CR>,<LF> */
        //At this time sum is 696 (0xfc+0xcd+0xef)
        byte [] frame = { (byte)0xff,(byte)0x03,(byte)0xfc,(byte)0xcd,(byte)0xef,(byte)(0xff-(0xfc+0xcd+0xef)%0xff), (byte)0x0d, (byte)0x0a };

        //Given
        byte[] inputOneFrame = frame;
        CyclicBufferProcessor processor = new CyclicBufferProcessor(null);

        //When
        processor.processInput(inputOneFrame, frame.length);

        //Then
        assertEquals(processor.stackSize(), 1);
        byte [] f = processor.pop();
        assertEquals(frame[1]+2, f.length);
        assertEquals(frame[2],   f[2]);
        assertEquals(frame[3],   f[3]);
        assertEquals(frame[4],   f[4]);

    }

    public void test_twoIncomingFrames(){

        byte [] frame1 = { (byte)0xff,(byte)0x03,(byte)0xfc,(byte)0xcd,(byte)0xef,(byte)(0xff-(0xfc+0xcd+0xef)%0xff), (byte)0x0d, (byte)0x0a };
        byte [] frame2 = { (byte)0xff,(byte)0x02,(byte)0xfc,(byte)0xcd,(byte)(0xff-(0xfc+0xcd)%0xff), (byte)0x0d, (byte)0x0a };

        //Given
        CyclicBufferProcessor processor = new CyclicBufferProcessor(null);

        //When
        processor.processInput(frame1, frame1.length);
        processor.processInput(frame2, frame2.length);

        //Then :
        assertEquals(processor.stackSize(), 2);
        byte [] f = processor.pop(); //frame1 should be first
        assertEquals(1, processor.stackSize());
        assertEquals(frame1[1]+2, f.length);
        assertEquals(frame1[2],   f[2]);
        assertEquals(frame1[3], f[3]);
        assertEquals(frame1[4], f[4]);
        f = processor.pop();  //frame2 is second
        assertEquals(0, processor.stackSize());
        assertEquals(frame2[1]+2, f.length);
        assertEquals(frame2[2],   f[2]);
        assertEquals(frame2[3],   f[3]);

        assertEquals(frame1.length+frame2.length, processor.length());
    }

    public void test_IncomingFrameInMultipleParts(){

        byte [] frame_p1 = {(byte)0xff,(byte)0x03};
        byte [] frame_p2 = {(byte)0xfc,(byte)0xcd,(byte)0xef};
        byte [] frame_p3 = {(byte)(0xff-(0xfc+0xcd+0xef)%0xff)};
        byte [] frame_p4 = {(byte)0x0d, (byte)0x0a};

        //Given
        CyclicBufferProcessor processor = new CyclicBufferProcessor(null);

        //When
        processor.processInput(frame_p1, frame_p1.length);
        processor.processInput(frame_p2, frame_p2.length);
        processor.processInput(frame_p3, frame_p3.length);
        processor.processInput(frame_p4, frame_p4.length);

        //Then
        assertEquals(1, processor.stackSize());
        byte [] f = processor.pop(); //frame1 should be first
        assertEquals(frame_p2[0], f[2+0]);
        assertEquals(frame_p2[1], f[2+1]);
        assertEquals(frame_p2[2], f[2+2]);
    }

    public void test_bufferTurnedForBigBuffers(){

        byte [] bigBuffer = new byte[80];
        bigBuffer[0] = (byte)0xff;
        bigBuffer[1] = (byte)75;

        bigBuffer[78] = (byte)0x0d;
        bigBuffer[79] = (byte)0x0a;
        int sum = 0;
        for(int i=0; i<76; i++){
            sum += i;
            bigBuffer[i+2] = (byte)i;
        }
        byte crc = (byte) (0xff-sum%0xff);
        bigBuffer[77] = crc;
        byte [] bigBuffer2 = new byte[80];
        System.arraycopy( bigBuffer, 0, bigBuffer2, 0, bigBuffer.length );

        //Given
        CyclicBufferProcessor processor = new CyclicBufferProcessor(null);

        //When
        processor.processInput(bigBuffer, bigBuffer.length);
        processor.processInput(bigBuffer2, bigBuffer2.length);

        //Then
        assertEquals(2, processor.stackSize());
        byte [] f = processor.pop();
        assertEquals(80-3, f.length);
        f = processor.pop();
        assertEquals(80-3, f.length);
        assertEquals(160-128, processor.length());
    }

    public void test_manyIncomingTapsFrames(){
        final Integer TEST_CNT = 100;
        byte [] tap_f = new byte[]{ (byte)0xff, (byte)0x03, (byte)0x01, (byte)0x00, (byte)0x02, (byte)0xfc, (byte)0x0d, (byte)0x0a};
        TapCollectorMock mock = new TapCollectorMock();
        CyclicBufferProcessor processor = new CyclicBufferProcessor(mock);

        for(int i=0; i<TEST_CNT; i++){
            if(i == 16){
                System.out.println("catch");
            }
            processor.processInput(tap_f.clone(), tap_f.length);
        }

        assertEquals(TEST_CNT, mock.tapCnt);
    }

    public void test_manyTapsInSeparateFrames(){
        final Integer TEST_CNT = 100;
        byte [] tap_fp1 = new byte[]{ (byte)0xff, (byte)0x03, (byte)0x01, (byte)0x00 };
        byte [] tap_fp2 = new byte[]{                                                (byte)0x02, (byte)0xfc, (byte)0x0d, (byte)0x0a};

        TapCollectorMock mock = new TapCollectorMock();
        CyclicBufferProcessor processor = new CyclicBufferProcessor(mock);

        for(int i=0; i<TEST_CNT; i++){
            processor.processInput(tap_fp1.clone(), tap_fp1.length);
            processor.processInput(tap_fp2.clone(), tap_fp2.length);
        }

        assertEquals(TEST_CNT, mock.tapCnt);
    }

}
