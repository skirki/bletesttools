package info.androidhive.functional;

import android.bluetooth.BluetoothGattService;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class BleProfile implements List<BluetoothGattService> {

    private List<BluetoothGattService> services;

    @Override
    public void add(int i, BluetoothGattService bluetoothGattService) {
        services.add(i, bluetoothGattService);
    }

    @Override
    public boolean add(BluetoothGattService bluetoothGattService) {
        return services.add(bluetoothGattService);
    }

    @Override
    public boolean addAll(int i, Collection<? extends BluetoothGattService> bluetoothGattServices) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends BluetoothGattService> bluetoothGattServices) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> objects) {
        return false;
    }

    @Override
    public boolean equals(Object o) {
        return false;
    }

    @Override
    public BluetoothGattService get(int i) {
        return null;
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public Iterator<BluetoothGattService> iterator() {
        return null;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<BluetoothGattService> listIterator() {
        return null;
    }

    @Override
    public ListIterator<BluetoothGattService> listIterator(int i) {
        return null;
    }

    @Override
    public BluetoothGattService remove(int i) {
        return null;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> objects) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> objects) {
        return false;
    }

    @Override
    public BluetoothGattService set(int i, BluetoothGattService bluetoothGattService) {
        return null;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public List<BluetoothGattService> subList(int i, int i2) {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        return null;
    }
}
