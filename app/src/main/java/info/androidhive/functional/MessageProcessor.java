package info.androidhive.functional;

import info.androidhive.actionbar.BleBroadcast;
import info.androidhive.ble.BleBackgroundBroadcaster;

/**
 * Created by hjacker on 30.05.15.
 */
public class MessageProcessor {

    private static final byte TEST_MARKER = 0;
    private static final byte EVENT_MARKER = 1;
    private static final byte WRITE_MARKET = 2;
    private static final byte READ_MARKET = 3;

    private static final byte ACC_DRIVER_FILE = 1;
    private static final byte ACC_DATA_FILE = 2;
    private static final byte BLE_DRIVER = 3;
    private static final byte RTC_DRIVER = 5;
    private static final byte RTC_DATA = 6;

    BleBroadcast broadcast;

    public MessageProcessor(){
        broadcast = new BleBackgroundBroadcaster();
    }


    public void processFrame(byte [] frame){
        if(broadcast == null) return;

        byte marker = frame[2];

        switch(marker){
            case TEST_MARKER  : onPingTest(frame); break;
            case EVENT_MARKER : onIncomingEvent(frame); break;
            case WRITE_MARKET : onWriteEvent(frame); break;
            case READ_MARKET  : onReadEvent(frame); break;
        }
    }

    private void validateFrame(byte [] frame){

    }

    private void onPingTest(byte [] f){

    }

    private void onIncomingEvent(byte [] f){
        //STE
        if(f[3] == (byte)0){
            //TAP
            broadcast.doBroadcastOnTapDetected(f);
        }
    }

    private void onWriteEvent(byte [] f){

    }

    private void onReadEvent(byte [] f){

        onReadPedometerData(f);
        return;

//        byte marker = f[3];
//        switch(marker){
//            case ACC_DRIVER_FILE    : onReadPedometerDriver(f); break;
//            case ACC_DATA_FILE      : onReadPedometerData(f); break;
//            case BLE_DRIVER         : onReadBleDriver(f); break;
//            case RTC_DRIVER         : onReadRtcDriver(f); break;
//            case RTC_DATA           : onReadRtcData(f); break;
//        }


    }

    private void onReadRtcData(byte[] f) {

    }

    private void onReadRtcDriver(byte[] f) {

    }

    private void onReadPedometerData(byte [] f){
        broadcast.doBroadcastOnReadPedometerData(f);
    }

    private void  onReadPedometerDriver(byte [] f){

    }

    private void onReadBleDriver(byte [] f) {

    }

}
