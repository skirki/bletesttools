package info.androidhive.actionbar;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.Calendar;

import info.androidhive.ble.BleService;
import info.androidhive.charts.Line;
import info.androidhive.charts.LineGraph;
import info.androidhive.charts.LinePoint;
import info.androidhive.communication.AppMessages;
import info.androidhive.communication.BleServiceIPC_Messages;

public class TargetAndGoalsActivity_5 extends Activity {

    private static Context context;

    private final int [] initial_data = {5, 14, 5, 4, 3, 5, 5, 9, 8, 7, 16, 2, 1, 6, 8, 18, 7, 9, 3, 3, 10, 5, 2, 14, 0, 0, 0, 15, 7, 9, 11, 2, 6, 0, 0, 29, 6, 3, 2, 6, 3, 2, 11, 4, 3, 6, 5, 13, 5, 2, 0, 3, 2, 7, 5, 1, 9, 8, 3, 1, 0, 8, 9, 8, 5, 9, 1, 11, 4, 5, 7, 4};
    final int ACC_DRIVER_FILE_SIZE = 16;
    byte [] accDriverFileReadPayload = ("STR 16 2 0\r\n").getBytes();

    byte [] accDriverConfigurationReadPayload = "STR 14 1 0\r\n".getBytes();
    final int ACC_DRIVER_CONFIGURATION_SIZE = 14;

    boolean isBound = false;
    Messenger mMessenger;

    private Handler mHandler = new Handler();

    void doBindService() {
        Intent intent = new Intent(this, BleService.class);
        bindService(intent, bleServiceConnection, Context.BIND_AUTO_CREATE);
    }

    void doUnbindService() {
        if (isBound) {
            // Detach our existing connection.
            unbindService(bleServiceConnection);
            isBound = false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(BleServiceIPC_Messages.NOTIFICATION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    private void updatePlot(){
        final byte [] read_flash_payload = "STR 7 7 0\r\n".getBytes();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                writeToTargetCharacteristic(read_flash_payload);
            }
        }, 2000);

    }

    private ServiceConnection bleServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            isBound = true;

            // Create the Messenger object
            mMessenger = new Messenger(service);

            Toast.makeText(context, R.string.ble_service_connected, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Toast.makeText(context, R.string.local_service_disconnected, Toast.LENGTH_SHORT).show();
            // unbind or process might have crashes
            mMessenger = null;
            isBound = false;
        }
    };

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action5);
        TextView text = (TextView) findViewById(R.id.NotificationTitle);
        context = this;

        doBindService();

        configurePushButtons();
        updateTime();
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        doUnbindService();
    }

    private void configurePushButtons() {

        Button button = (Button) findViewById(R.id.refreshFitnessValuesButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshFitnessValues();
                updatePlot();
            }
        });

    }

    private void updateDriverConf(byte [] byteArray){

        IntBuffer intBuf =
                ByteBuffer.wrap(byteArray)
                        .order(ByteOrder.BIG_ENDIAN)
                        .asIntBuffer();
        final int[] array = new int[intBuf.remaining()];
        intBuf.get(array);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                String respons = "Response: ";

                for(int i=0; i<array.length; i++){
                    respons += " "+array[i];
                }

                Toast.makeText(context, respons, Toast.LENGTH_LONG).show();
            }
        });

        //TODO: fix this shit out
        //chaining z d
        readAccDriverConfiguration();
    }

    private void readAccDriverConfiguration(){
        writeToTargetCharacteristic(accDriverConfigurationReadPayload);
    }
static int steps = 0;
    private void updateDriverData(byte [] byteArray){

        if(byteArray.length<16)
            return;
        final int offset = 5;
        int x_h = (byteArray[0+offset]& 0xFF)<<8;
        int x_l = byteArray[1+offset]& 0xFF;
        final int acc_x = x_h + x_l;
        int y_h = (byteArray[2+offset]& 0xFF)<<8;
        int y_l = byteArray[3+offset]& 0xFF;
        final int acc_y = y_h + y_l;
        int z_h = (byteArray[4+offset]& 0xFF)<<8;
        int z_l = byteArray[5+2]& 0xFF;
        final int acc_z = z_h + z_l;
        final int acc_activity = (int)byteArray[7+offset];
        int step_msb = (byteArray[8+offset]& 0xFF)<<8;
        int step_lsb = byteArray[9+offset]& 0xFF;
        final int acc_step_cnt =  step_lsb + step_msb;
        int distance_msb = (byteArray[10+offset]& 0xFF)<<8;
        int distance_lsb = byteArray[11+offset]& 0xFF;
        final int acc_distance =  distance_lsb + distance_msb;
        int speed_msb = (byteArray[12+offset]& 0xFF)<<8;
        int speed_lsb = byteArray[13+offset]& 0xFF;
        final int acc_speed    =  speed_msb + speed_lsb;
        int calories_msb = (byteArray[14+offset]& 0xFF)<<8;
        int calories_lsb = byteArray[15+offset]& 0xFF;
        final int acc_calories =  calories_msb + calories_lsb;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                TextView text = (TextView) findViewById(R.id.textView53);
                text.setText( "Calories: " + acc_calories);

                text = (TextView) findViewById(R.id.textView40);
                text.setText( "Distance: " + acc_distance);

                text = (TextView) findViewById(R.id.textView42);
                text.setText( "Steps: " + acc_step_cnt);
                steps = acc_step_cnt;
                text = (TextView) findViewById(R.id.textViewActivity);
                if(acc_activity == 0x1)
                    text.setText( "Activity: " + "Rest");
                else if(acc_activity == 0x2)
                    text.setText( "Activity: " + "Walking");
                else if(acc_activity == 0x3)
                    text.setText( "Activity: " + "Joging");
                else if(acc_activity == 0x4)
                    text.setText( "Activity: " + "Run");

                text = (TextView) findViewById(R.id.textViewSpeed);
                text.setText( "Speed: " + acc_speed);


                String response = "X:" + acc_x + " Y:" + acc_y + " Z:" + acc_z +
                        " act:" + acc_activity + " step:" + acc_step_cnt + " dist:" + acc_distance +
                        " speed:" + acc_speed;

                Toast.makeText(context, response, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void readAccDriverData(){
        writeToTargetCharacteristic(accDriverFileReadPayload);
    }

    private void refreshFitnessValues(){
        readAccDriverData();
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                String action = bundle.getString(BleServiceIPC_Messages.ACTION);
                int resultCode = bundle.getInt(BleServiceIPC_Messages.RESULT);
                if(action==null) return;

                if(AppMessages.ACC_PEDOMETER_TARGET_UPDATED.equals(action)){
                    byte [] response = (byte []) bundle.get(BleServiceIPC_Messages.OBJECT);

                    if(response.length < ACC_DRIVER_FILE_SIZE) return;

                    updateDriverData(response);
                }
                if(AppMessages.ACTION_FLASH_DATA_READ.equals(action)){
                    byte [] response = (byte []) bundle.get(BleServiceIPC_Messages.BYTE_ARRAY);

                    if(response.length < 7) return;

                    writePlot(response);
                }

            }
        }
    };

    static int lastMinutes = 0;
    protected void writePlot(byte [] payload){
        Calendar c = Calendar.getInstance();

        int seconds = c.get(Calendar.SECOND);
        int minutes = c.get(Calendar.MINUTE);
        int hour = c.get(Calendar.HOUR_OF_DAY);

        for(int i=0; i<6; i++){
            initial_data[initial_data.length-i-2] = Math.abs( (payload[11 - i] & 0xFF)-(payload[11 - i -1] & 0xFF) );
        }

        initial_data[initial_data.length-1] = steps - (payload[5 + 0] & 0xFF);

        Line l = new Line();
        for(int i=0; i<72; i++){
            LinePoint d = new LinePoint();
            d.setX(i);
            d.setY(initial_data[i]);
            //d.setY((payload[5 + i] & 0xFF) +i);
            l.addPoint(d);
        }

        l.setColor(Color.parseColor("#FFBB33"));
        l.setShowingPoints(false);

        LineGraph g = (LineGraph)findViewById(R.id.graph);
        g.removeAllLines();
        g.addLine(l);

        int max = 0;
        for(int i=0; i<72; i++){
            max = Math.max(max, initial_data[i]);
        }

        g.setRangeY(0, max);

        g.setLineToFill(0);

        TextView textLeft  = (TextView) findViewById(R.id.textChartLeft);
        TextView textRight = (TextView) findViewById(R.id.textChartRight);
        String min;
        if(minutes > 9)
            min = ""+minutes;
        else
            min= "0"+minutes;

        textRight.setText(hour+":"+min);
        textLeft.setText((hour-1)+":"+min);
    }

    protected void writeToTargetCharacteristic(byte [] payload){

        Message msg = Message.obtain(null, BleServiceIPC_Messages.MSG_WRITE_CHARACTERISTIC_TO_DEVICE, 0, 0);
        Bundle bundle = new Bundle();
        bundle.putSerializable(BleServiceIPC_Messages.BYTE_ARRAY, payload);
        msg.setData(bundle);
        try {
            mMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void updateTime(){
        if(!isFinishing())
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {

                for (int i = 0; i < initial_data.length - 2; i++) {
                    initial_data[i] = initial_data[i+1];
                }
                updateTime();
            }
        }, 60000 );

    }
}
