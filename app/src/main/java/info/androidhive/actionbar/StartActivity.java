package info.androidhive.actionbar;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import info.androidhive.actionbar.adapter.TitleNavigationAdapter;
import info.androidhive.actionbar.model.LocationFound;
import info.androidhive.actionbar.model.SpinnerNavItem;
import info.androidhive.ble.BleService;
import info.androidhive.communication.AppMessages;
import info.androidhive.communication.BleServiceIPC_Messages;
import info.androidhive.functional.BleDevice;

public class StartActivity extends Activity
        implements ActionBar.OnNavigationListener,
                    DialogInterface.OnClickListener,
                    SwipeRefreshLayout.OnRefreshListener {

    //public
    public PersonalData pd;

    // action bar
	private ActionBar actionBar;

	// Title navigation Spinner data
	private ArrayList<SpinnerNavItem> navSpinner;

	// Navigation adapter
	private TitleNavigationAdapter adapter;

	// Refresh menu item
	private MenuItem refreshMenuItem;

    // list view
    private ListView listView;

    private static Context context;
    private static StartActivity that;

    private List<BleDevice> latestFoundDevices;

    private boolean mIsBound = false;

    private MenuItem menuItem;
    private String [] macAddresses = {"00:07:80:2d:ad:31", "00:07:80:2d:c8:5d", "00:07:80:2d:a6:bc", "00:07:80:2d:ad:2a", "00:07:80:2d:ad:2b", "00:07:80:2d:a6:be", "00:07:80:2d:ad:28", "00:07:80:2d:c8:5b", "00:07:80:2d:ad:2f", "00:07:80:2d:c8:5e", "00:07:80:2d:ad:1c", "00:07:80:2d:a6:bb", "00:07:80:2d:c8:5c", "00:07:80:2d:c8:4d", "00:07:80:2d:c8:4b", "00:07:80:2d:a6:c5", "00:07:80:2d:a6:c4", "00:07:80:2d:ac:e9", "00:07:80:2d:ad:29", "00:07:80:2d:ac:ee", "00:07:80:05:e0:D7", "00:07:80:05:e0:d9", "00:07:80:05:e0:df" };
    private List<String> validDevices = new ArrayList<>(Arrays.asList(macAddresses));
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.start_screen);

        pd = new PersonalData();

		actionBar = getActionBar();

		// Hide the action bar title
		actionBar.setDisplayShowTitleEnabled(false);

		// Enabling Spinner dropdown navigation
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

		// Spinner title navigation data
		navSpinner = new ArrayList<SpinnerNavItem>();
		navSpinner.add(new SpinnerNavItem("Local", R.drawable.ic_location));
		navSpinner.add(new SpinnerNavItem("My Places", R.drawable.ic_my_places));
		navSpinner.add(new SpinnerNavItem("Checkins", R.drawable.ic_checkin));
		navSpinner.add(new SpinnerNavItem("Latitude", R.drawable.ic_latitude));

		// title drop down adapter
		adapter = new TitleNavigationAdapter(getApplicationContext(),
				navSpinner);

		// assigning the spinner navigation
		actionBar.setListNavigationCallbacks(adapter, this);

		// Changing the action bar icon
		// actionBar.setIcon(R.drawable.ico_actionbar);
        context = this;
        that = this;

        // List Usage example
        listView = getListView();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String item = (String)listView.getAdapter().getItem(position);

                Toast.makeText(context, "Connecting to.. " + item, Toast.LENGTH_SHORT).show();

                BleDevice target = latestFoundDevices.get(position);

                requestConnectToBleDevice(target);
            }
        });

        BluetoothAdapter bt = BluetoothAdapter.getDefaultAdapter();
        if (bt == null){
            //Does not support Bluetooth .setText("Your device does not support Bluetooth");
            return;
        }else{
            //Magic starts. Let's check if it's enabled
            if (!bt.isEnabled()){
                enableBleRequest();
            }
        }

        Intent intent = new Intent(this, BleService.class);
        bindService(intent, bleServiceConnection, Context.BIND_AUTO_CREATE);

        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
	}

    private ListView getListView(){
        return (ListView)findViewById(R.id.listViewScanned);
    }

    SwipeRefreshLayout swipeLayout;
    @Override public void onRefresh() {

        if(!isBound)
            return;

        refreshDevices();
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                swipeLayout.setRefreshing(false);
            }
        }, 5000);
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_main_actions, menu);

		// Associate searchable configuration with the SearchView
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView) menu.findItem(R.id.action_search)
				.getActionView();
		searchView.setSearchableInfo(searchManager
				.getSearchableInfo(getComponentName()));

		return super.onCreateOptionsMenu(menu);
	}

	/**
	 * On selecting action bar icons
	 * */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Take appropriate action for each action item click
		switch (item.getItemId()) {
		case R.id.action_search:
			// search action
			return true;
		case R.id.action_location_found:
			// location found
			LocationFound();
			return true;
		case R.id.action_refresh:
			// refresh
            menuItem = item;
            menuItem.setActionView(R.layout.action_progressbar);
            menuItem.expandActionView();
            refreshDevices();
			return true;
		case R.id.action_help:
			// help action
			return true;
		case R.id.action_check_updates:
			// check for updates action
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Launching new activity
	 * */
	private void LocationFound() {
		Intent i = new Intent(StartActivity.this, LocationFound.class);
		startActivity(i);
	}

	/*
	 * Actionbar navigation item select listener
	 */
	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		// Action to be taken after selecting a spinner item
		return false;
	}

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {

    }

    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
            List<String> objects) {
                super(context, textViewResourceId, objects);
                for (int i = 0; i < objects.size(); ++i) {
                    mIdMap.put(objects.get(i), i);
                }
        }

        @Override
        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }

    private void refreshDevices() {

        requestBleScan();
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                String action = bundle.getString(BleServiceIPC_Messages.ACTION);
                int resultCode = bundle.getInt(BleServiceIPC_Messages.RESULT);

                if (resultCode == AppMessages.BLUETOOTH_NOT_ENABLED) {
                    enableBleRequest();
                }
                if(action==null) return;

                if(AppMessages.SCAN_END.equals(action)){
                    onScanFinished(bundle.get(BleServiceIPC_Messages.OBJECT));
                }
                else if(AppMessages.SCAN_START.equals(action)){
                    //Update refresh icon
                }
                else if(AppMessages.CONNECTION_TO_NOT_VALID_DEVICE.equals(action)){
                    connectionToNotValidDevice();
                }
                else if( action.equals(AppMessages.SUCCESSFULLY_CONNECTED_TO_DEVICE)){
                    String deviceName = bundle.getString(AppMessages.CONNECTED_TO);
                    connectedToDevice(deviceName);
                }
                else if (action.equals(AppMessages.BLE_DEVICE_DISCONNECTED)) {

                    onDeviceDisconnected();
                }
            }
        }
    };

    private void onScanFinished(Object result){
        Object obj = result;
        if(obj != null){
            latestFoundDevices = (ArrayList<BleDevice>) obj;
        }

        onScanEnd();
    }

    private void onScanEnd(){
        updateDevicesNearby();
        //Update refresh icon
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(BleServiceIPC_Messages.NOTIFICATION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    void doUnbindServices() {
        if (mIsBound) {
            // Detach our existing connection.
            unbindService(bleServiceConnection);
            mIsBound = false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        doUnbindServices();
    }


    /*
     * Action Events: Action caused by received broadcasts
     */
    protected void connectedToDevice(String deviceName){
        final String msg = "Connected to device";
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();

        clearDevicesNearby();

        onDeviceConnected();
    }

    protected void connectionToNotValidDevice(){
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Connection Error");
        alertDialog.setMessage("You are trying to connect to non valid device. Connection discarded.");
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // here you can add functions
            }
        });
         alertDialog.setIcon(R.drawable.ic_action_action_report_problem);
         alertDialog.show();
    }

    protected void enableBleRequest(){
        Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableIntent, 1);
    }

    protected void updateDevicesNearby(){
        if(latestFoundDevices == null)
            return;

        List<String> devicesNames = new ArrayList<String>();

        if(latestFoundDevices.size() > 0) {
            setTitleTextConnectToDevice();
        }

        for(BleDevice device : latestFoundDevices){

            for(String d : validDevices){
                String mac = device.getMacAddress();
                if(d.equalsIgnoreCase(mac)){
                    devicesNames.add(device.getDeviceName() + " - " + device.getMacAddress());
                }
            }
        }

        listView = getListView();
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listView.setTextFilterEnabled(true);
        final StableArrayAdapter adapter = new StableArrayAdapter(context, android.R.layout.simple_list_item_checked, devicesNames);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void setTitleTextNoDevices(){
        TextView tv = (TextView)findViewById(R.id.textView);
        tv.setText(R.string.no_wearable);
    }

    private void setTitleTextConnectToDevice(){
        TextView tv = (TextView)findViewById(R.id.textView);
        tv.setText("Click on Your device to connect");
    }

    protected void clearDevicesNearby(){
        latestFoundDevices = null;
        listView = getListView();
        final StableArrayAdapter adapter = new StableArrayAdapter(context, android.R.layout.simple_list_item_checked, new ArrayList<String>());
        listView.setAdapter(adapter);
    }

    public static void returnToUiThread(Runnable task){
        that.runInMainThread(task);
    }

    public void runInMainThread(Runnable task){
        runOnUiThread(task);
    }

    private void onDeviceConnected() {
        Intent myIntent = new Intent(StartActivity.this, GlobalActivity.class);
        StartActivity.this.startActivity(myIntent);
    }

    private void onDeviceDisconnected() {

    }

    /**
     * Ble IPC Requests
     */
    private void requestBleScan(){
        // Create a Scan Message
        Message msg = Message.obtain(null, BleServiceIPC_Messages.MSG_SCAN_BLE_DEVICES, 0, 0);

        // Create a bundle with the data
        Bundle bundle = new Bundle();
        bundle.putString("hello", "world");

        // Set the bundle data to the Message
        msg.setData(bundle);

        // Send the Message to the Service (in another process)
        try {
            mMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void requestConnectToBleDevice(BleDevice target){
        String macAddress = target.getMacAddress();

        Message msg = Message.obtain(null, BleServiceIPC_Messages.MSG_CONNECT_TO_DEVICE, 0, 0);
        Bundle bundle = new Bundle();
        bundle.putString(BleServiceIPC_Messages.MAC_ADDRESS, macAddress);
        msg.setData(bundle);
        try {
            mMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }


    boolean isBound = false;
    Messenger mMessenger;

    private ServiceConnection bleServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            isBound = true;

            // Create the Messenger object
            mMessenger = new Messenger(service);

            // Create a Message
            Message msg = Message.obtain(null, BleServiceIPC_Messages.MSG_BIND_SERVICE, 0, 0);

            // Create a bundle with the data
            Bundle bundle = new Bundle();
            bundle.putString("hello", "BleService bound");

            // Set the bundle data to the Message
            msg.setData(bundle);

            // Try to Send the Message to the Service (in another process)
            try {
                mMessenger.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            // unbind or process might have crashes
            mMessenger = null;
            isBound = false;
        }
    };

}
