package info.androidhive.actionbar;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import info.androidhive.widgets.DateTimePicker;

public class AlarmActivity_4 extends AbstractCommunicationActivity {

    private static Context context;
    private boolean mIsBound = false;

    private String mDate;
    private String mTime;

    private Integer chosenMask = 0;
    private Integer chosenWeekDaySel = 0;
    private Integer chosenWeekDay = 0;

    private final String _space = " ";

    static String[] alarm_mask = new String[]{
            "AlarmMask_None",
            "AlarmMask_DateWeekDay",
            "AlarmMask_Hours",
            "AlarmMask_Minutes",
            "AlarmMask_Seconds",
            "AlarmMask_All"
    };

    static String[] date_week_day_sel = new String[]{
            "AlarmDateWeekDaySel_Date",
            "AlarmDateWeekDaySel_WeekDay"
    };

    static String[] date_week_day = new String[]{
            "Weekday_Monday",
            "Weekday_Tuesday",
            "Weekday_Wednesday",
            "Weekday_Thursday",
            "Weekday_Friday",
            "Weekday_Saturday",
            "Weekday_Sunday"
    };

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action4);
        TextView text = (TextView) findViewById(R.id.NotificationTitle);
        context = this;

        configurePushButtons();
    }

    @Override
    void onBleServiceConnected() {

    }

    @Override
    void onBleServiceDisconnected() {

    }

    @Override
    void onReturnFromActivity() {

    }

    @Override
    void onTapReceived(byte [] f) {

    }

    @Override
    void onRtcDriverRead(byte [] f) {

    }

    @Override
    void onRtcDataRead(byte [] f) {
//        byte [] response = responseContent.getContent();
//
//        if(response.length < 8) return;
//
//        final Map<String, Integer> dateTime = getParameters(response, 17);
//
//        updateDateTime(dateTime);
    }

    @Override
    void onAccDataRead(byte [] f) {

    }

    @Override
    void onAccDriverRead(byte [] f) {

    }

    private void updateAlarmDateField(String date){
        TextView textView = (TextView) findViewById(R.id.textViewAlarmDate);

        textView.setText(date);
    }

    private void updateAlarmTimeField(String time){
        TextView textView = (TextView) findViewById(R.id.textViewAlarmTime);

        textView.setText(time);
    }

    private void configureDatePicker(){
        TextView textViewAlarmDate = (TextView) findViewById(R.id.textViewAlarmDate);

        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

            private void updateLabel() {

                String myFormat = "MM/dd/yy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                Toast.makeText(AlarmActivity_4.this, sdf.format(myCalendar.getTime()), Toast.LENGTH_SHORT).show();
                updateAlarmDateField(sdf.format(myCalendar.getTime()));
            }

        };

        textViewAlarmDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(AlarmActivity_4.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void configureTimePicker(){
        final TextView textViewAlarmTime = (TextView) findViewById(R.id.textViewAlarmTime);

        textViewAlarmTime.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(AlarmActivity_4.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String selectedTime = selectedHour + ":" + selectedMinute;
                        Toast.makeText(AlarmActivity_4.this, selectedHour + ":" + selectedMinute, Toast.LENGTH_SHORT).show();
                        updateAlarmTimeField(selectedTime);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });
    }

    private void configureWriteAlarmButton() {
        final Button button = (Button) findViewById(R.id.writeAlarmButton);

        button.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                showDateTimeDialog();
            }
        });
    }

    private void configureTurnAlarmButton() {
        final Button button = (Button) findViewById(R.id.turnAlarmButton);

        button.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

            }
        });
    }

    private void configureReadAlarmButton() {
        final Button button = (Button) findViewById(R.id.readAlarmButton);

        button.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                int bytesToRead = 1 + 4 * 4;
                byte [] readPayload = ("STR "+bytesToRead+" 6 3\r\n").getBytes();

                writeToTargetCharacteristic(readPayload);
            }
        });
    }

    private void configurePushButtons() {
        configureDatePicker();
        configureTimePicker();
        configureReadAlarmButton();
        configureTurnAlarmButton();
        configureWriteAlarmButton();
        configureRefreshButton();

        configDateWeekDay();
        configDateWeekDaySel();
        configAlarmMaskTextView();
    }

    private void updateDateTime(final Map<String, Integer> dateTime){

        Runnable updateDateTime = new Runnable() {
            @Override
            public void run() {

                final int hours   = dateTime.get("hours");
                final int minutes = dateTime.get("minutes");
                final int seconds = dateTime.get("seconds");
                int timeBase= dateTime.get("timeBase");

                TextView text = (TextView) findViewById(R.id.alarmTimeTextView);
                String tbc;
                if(timeBase == 0){
                    tbc = "PM";
                } else {
                    tbc = "AM";
                }

                text.setText(hours+":"+minutes+":"+seconds+":"+tbc);

                int weekDay = dateTime.get("weekDay");
                int month = dateTime.get("month");
                int date = dateTime.get("date");
                int year = dateTime.get("year");

                String datDescription = null;
                int day = convertStmToJavaDayOfWeek(weekDay);
                switch(day){
                    case 1 : datDescription = "Sun"; break;
                    case 2 : datDescription = "Mon"; break;
                    case 3 : datDescription = "Tue"; break;
                    case 4 : datDescription = "Wed"; break;
                    case 5 : datDescription = "Thu"; break;
                    case 6 : datDescription = "Fri"; break;
                    case 7 : datDescription = "Sat"; break;
                }

                final TextView textView = (TextView) findViewById(R.id.alarmDateTextView);
                String displayDate = (month)+"/"+date+"/"+year + " -- " + datDescription;
                textView.setText(displayDate);

                int alarm_hours = dateTime.get("alarm_hours");
                int alarm_minutes = dateTime.get("alarm_minutes");
                int alarm_seconds = dateTime.get("alarm_seconds");
                timeBase = dateTime.get("alarm_timeBase");
                if(timeBase == 0){
                    tbc = "PM";
                } else {
                    tbc = "AM";
                }

                final TextView textViewAlarmTime = (TextView) findViewById(R.id.textViewAlarmTime);
                textViewAlarmTime.setText(alarm_hours+":"+alarm_minutes+":"+alarm_seconds+":"+tbc);

                final TextView alarmMaskTextView = (TextView) findViewById(R.id.alarmMaskTextView);
                Integer alarm_Mask = dateTime.get("alarmMask");
                String displayAlarmMask = null;
                if(     alarm_Mask == 0x00000000){ displayAlarmMask = "AlarmMask_None"; }
                else if(alarm_Mask == 0x80000000){ displayAlarmMask = "AlarmMask_DateWeekDay"; }
                else if(alarm_Mask == 0x00800000){ displayAlarmMask = "AlarmMask_Hours"; }
                else if(alarm_Mask == 0x00008000){ displayAlarmMask = "AlarmMask_Minutes"; }
                else if(alarm_Mask == 0x00000080){ displayAlarmMask = "AlarmMask_Seconds"; }
                else if(alarm_Mask == 0x80808080){ displayAlarmMask = "AlarmMask_All"; }

                alarmMaskTextView.setText(displayAlarmMask);

                final TextView alDateWeekDaySel = (TextView) findViewById(R.id.alarmDateWeekDaySel);
                Integer alarm_Date_Week_DaySel = dateTime.get("alarmDateWeekDaySel");
                String displayAlarmDateWeekDaySel = null;
                if(     alarm_Date_Week_DaySel == 0x00000000){ displayAlarmDateWeekDaySel = "AlarmMask_None"; }
                else if(alarm_Date_Week_DaySel == 0x80000000){ displayAlarmDateWeekDaySel = "AlarmMask_DateWeekDay"; }
                else if(alarm_Date_Week_DaySel == -1073741824){ displayAlarmDateWeekDaySel = "AlarmMask_DateWeekDay"; }

                alDateWeekDaySel.setText(displayAlarmDateWeekDaySel);

                final TextView alarmDateWeekDayValue = (TextView) findViewById(R.id.alarmDateWeekDayValue);
                Integer alarm_Date_WeekDay = dateTime.get("alarmDateWeekDay");
                String displayAlarmDateWeekDay = null;
                if(     alarm_Date_WeekDay == 0x01){ displayAlarmDateWeekDay = "Weekday_Monday"; }
                else if(alarm_Date_WeekDay == 0x02){ displayAlarmDateWeekDay = "Weekday_Tuesday"; }
                else if(alarm_Date_WeekDay == 0x03){ displayAlarmDateWeekDay = "Weekday_Wednesday"; }
                else if(alarm_Date_WeekDay == 0x04){ displayAlarmDateWeekDay = "Weekday_Thursday"; }
                else if(alarm_Date_WeekDay == 0x05){ displayAlarmDateWeekDay = "Weekday_Friday"; }
                else if(alarm_Date_WeekDay == 0x06){ displayAlarmDateWeekDay = "Weekday_Saturday"; }
                else if(alarm_Date_WeekDay == 0x07){ displayAlarmDateWeekDay = "Weekday_Sunday"; }
                else if(alarm_Date_WeekDay == 0x09){ displayAlarmDateWeekDay = "Weekday_ALL"; }

                alarmDateWeekDayValue.setText(displayAlarmDateWeekDay);

            }
        };


        runOnUiThread(updateDateTime);
    }

    Integer convertStmToJavaDayOfWeek(int nr){
        int dayOfWeek = nr;
        if(dayOfWeek == 7) dayOfWeek = 1;
        else dayOfWeek += 1;

        return dayOfWeek;
    }


    Integer convertJavaToStmDayOfWeek(int nr){
        int dayOfWeek = nr;
        if(dayOfWeek == 1) dayOfWeek = 7;
        else dayOfWeek -= 1;

        return dayOfWeek;
    }

    private void configureRefreshButton(){
        final Button button = (Button) findViewById(R.id.refreshAlarmDateTimeButton);

        button.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                int pagesToRead = 8;

                byte [] readPayload = ("STR " + pagesToRead*4 + " 6 0\r\n").getBytes();

                final ResponseContent responseContent = new ResponseContent(pagesToRead*4);
                Runnable responseAction = new Runnable() {
                    @Override
                    public void run() {
                        byte [] response = responseContent.getContent();

                        if(response.length < 8) return;

                        final Map<String, Integer> dateTime = getParameters( response, 32);

                        updateDateTime(dateTime);
                    }
                };
                responseContent.setResponseAction(responseAction);
                writeToTargetCharacteristic(readPayload);

            }
        });
    }

    private void showDateTimeDialog() {
        // Create the dialog
        final Dialog mDateTimeDialog = new Dialog(this);
        // Inflate the root layout
        final RelativeLayout mDateTimeDialogView = (RelativeLayout) getLayoutInflater().inflate(R.layout.date_time_dialog, null);
        // Grab widget instance
        final DateTimePicker mDateTimePicker = (DateTimePicker) mDateTimeDialogView.findViewById(R.id.DateTimePicker);
        // Check is system is set to use 24h time (this doesn't seem to work as expected though)
        final String timeS = android.provider.Settings.System.getString(getContentResolver(), android.provider.Settings.System.TIME_12_24);
        final boolean is24h = !(timeS == null || timeS.equals("12"));

        // Update demo TextViews when the "OK" button is clicked
        ((Button) mDateTimeDialogView.findViewById(R.id.SetDateTime)).setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                mDateTimePicker.clearFocus();
                // TODO Auto-generated method stub
                mDate = (mDateTimePicker.get(Calendar.YEAR) + "/" + (mDateTimePicker.get(Calendar.MONTH)+1) + "/"
                        + mDateTimePicker.get(Calendar.DAY_OF_MONTH));
                if (mDateTimePicker.is24HourView()) {
                    mTime = (mDateTimePicker.get(Calendar.HOUR_OF_DAY) + ":" + mDateTimePicker.get(Calendar.MINUTE));
                } else {
                    mTime = (mDateTimePicker.get(Calendar.HOUR) + ":" + mDateTimePicker.get(Calendar.MINUTE) + " "
                            + (mDateTimePicker.get(Calendar.AM_PM) == Calendar.AM ? "AM" : "PM"));
                }

                String payload = createPayloadFrame(mDateTimePicker);
                writeToTargetCharacteristic(payload.getBytes());
                mDateTimeDialog.dismiss();
            }
        });

        // Cancel the dialog when the "Cancel" button is clicked
        ((Button) mDateTimeDialogView.findViewById(R.id.CancelDialog)).setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                mDateTimeDialog.cancel();
            }
        });

        // Reset Date and Time pickers when the "Reset" button is clicked
        ((Button) mDateTimeDialogView.findViewById(R.id.ResetDateTime)).setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                mDateTimePicker.reset();
            }
        });

        // Setup TimePicker
        mDateTimePicker.setIs24HourView(is24h);
        // No title on the dialog window
        mDateTimeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Set the dialog content view
        mDateTimeDialog.setContentView(mDateTimeDialogView);
        // Display the dialog
        mDateTimeDialog.show();
    }

    private Map<String, Integer> getParameters(byte [] response, int length){
        final Map<String, Integer> dateTime = new HashMap<String, Integer>();

        if(length < 0) return dateTime;
        int hours   = response[0];
        dateTime.put("hours", hours);

        if(length < 1) return dateTime;
        int minutes = response[1];
        dateTime.put("minutes", minutes);

        if(length < 2) return dateTime;
        int seconds = response[2];
        dateTime.put("seconds", seconds);

        if(length < 3) return dateTime;
        int timeBase= response[3];
        dateTime.put("timeBase", timeBase);

        if(length < 4) return dateTime;
        int weekDay = response[4];
        dateTime.put("weekDay", weekDay);

        if(length < 5) return dateTime;
        int month   = response[5];
        dateTime.put("month", month);

        if(length < 6) return dateTime;
        int date    = response[6];
        dateTime.put("date", date);

        if(length < 7) return dateTime;
        int year    = response[7];
        dateTime.put("year", year);

        if(length < 8) return dateTime;
        int alarm_hours   = response[8];
        dateTime.put("alarm_hours", alarm_hours);

        if(length < 9) return dateTime;
        int alarm_minutes = response[9];
        dateTime.put("alarm_minutes", alarm_minutes);

        if(length < 10) return dateTime;
        int alarm_seconds = response[10];
        dateTime.put("alarm_seconds", alarm_seconds);

        if(length < 11) return dateTime;
        int alarm_timeBase= response[11];
        dateTime.put("alarm_timeBase", alarm_timeBase);

        if(length < 15) return dateTime;
        int am_msb = ((int)response[15])<<24;
        int am_2sb = ((int)response[14])<<16;
        int am_3sb = ((int)response[13])<<8;
        int am_lsb = response[12];

        int alarmMask = (-1)*am_msb + (-1)*am_2sb + (-1)*am_3sb + (-1)*am_lsb;
        dateTime.put("alarmMask", alarmMask);

        if(length < 19) return dateTime;
        am_msb = ((int)response[19])<<24;
        am_2sb = ((int)response[18])<<16;
        am_3sb = ((int)response[17])<<8;
        am_lsb = response[16];

        int alarmDateWeekDaySel = (-1)*am_msb + (-1)*am_2sb + (-1)*am_3sb + (-1)*am_lsb;
        dateTime.put("alarmDateWeekDaySel", alarmDateWeekDaySel);

        if(length < 20) return dateTime;
        int alarmDateWeekDay = response[20];
        dateTime.put("alarmDateWeekDay", alarmDateWeekDay);

        return dateTime;
    }

    private void configAlarmMaskTextView(){
        final TextView action = (TextView) findViewById(R.id.alarmMaskTextView);

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Pick a alarm Mask");

        action.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                builder.setItems(alarm_mask, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        switch(which){
                            case 0: chosenMask = 0;         break;
                            case 1: chosenMask = 80000000;  break;
                            case 2: chosenMask = 800000;    break;
                            case 3: chosenMask = 8000;      break;
                            case 4: chosenMask = 80;        break;
                            case 5: chosenMask = 80808080;  break;
                        }
                        onMaskChosen(which);
                    }
                });
                builder.show();
            }
        });

    }

    private void onMaskChosen(int index){
        final TextView text = (TextView) findViewById(R.id.alarmMaskTextView);
        text.setText(chosenMask.toString() );
    }

    private void configDateWeekDaySel(){
        final TextView action = (TextView) findViewById(R.id.alarmDateWeekDaySel);

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Pick a AlarmDateWeekDaySel");

        action.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                builder.setItems(date_week_day_sel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        switch(which){
                            case 0: chosenWeekDaySel = 0;         break;
                            case 1: chosenWeekDaySel = 40;  break;
                        }
                        onDateWeekDaySel(which);
                    }
                });
                builder.show();
            }
        });

    }

    private void onDateWeekDaySel(int index){
        final TextView text = (TextView) findViewById(R.id.alarmDateWeekDaySel);
        text.setText(chosenWeekDaySel.toString() );
    }

    private void configDateWeekDay(){
        final TextView action = (TextView) findViewById(R.id.alarmDateWeekDayValue);

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Pick a alarm Date WeekDay Value");

        action.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                builder.setItems(date_week_day, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        switch(which){
                            case 0: chosenWeekDay = 1;  break;
                            case 1: chosenWeekDay = 2;  break;
                            case 2: chosenWeekDay = 3;  break;
                            case 3: chosenWeekDay = 4;  break;
                            case 4: chosenWeekDay = 5;  break;
                            case 5: chosenWeekDay = 6;  break;
                            case 6: chosenWeekDay = 7;  break;
                        }
                        onDateWeekDay(which);
                    }
                });
                builder.show();
            }
        });

    }

    private void onDateWeekDay(int index){
        final TextView text = (TextView) findViewById(R.id.alarmDateWeekDayValue);
        text.setText(chosenWeekDay.toString() );
    }

    private String createPayloadFrame(final DateTimePicker mDateTimePicker){

        Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance

//        calendar.get(Calendar.HOUR_OF_DAY); // gets hour in 24h format
        int currentHour = calendar.get(Calendar.HOUR);        // gets hour in 12h format
        int currentMinute = calendar.get(Calendar.MINUTE);
        int currentSecond = calendar.get(Calendar.SECOND);
        int currentTbc = 1;

        int currentAlarmYear  = calendar.get(Calendar.YEAR)-2000;
        int currentAlarmMonth = calendar.get(Calendar.MONTH)+1;
        int currentDayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        int currentDayOfWeek  = calendar.get(Calendar.DAY_OF_WEEK);

        currentDayOfWeek = convertJavaToStmDayOfWeek(currentDayOfWeek);

        int alarmHours   = mDateTimePicker.get(Calendar.HOUR);
        int alarmMinutes = mDateTimePicker.get(Calendar.MINUTE);
        int alarmSeconds = mDateTimePicker.get(Calendar.SECOND);
        int tbc = (mDateTimePicker.get(Calendar.AM_PM) == Calendar.AM ?  1 : 0);//"AM" : "PM"));


        String curTime = ""+currentHour+_space+currentMinute+_space+currentSecond+_space+currentTbc;
        String curDate = ""+currentDayOfWeek+_space+currentAlarmMonth+_space+currentDayOfMonth + _space+ currentAlarmYear;
        String alarmTime = "" + alarmHours +_space+ alarmMinutes +_space+ alarmSeconds +_space+ tbc;
        String alarmMask = toLittleEndianWithSpaces(chosenMask);
        String alarmDateWeekSel = toLittleEndianWithSpaces(chosenWeekDaySel);
        String weekday = "" + chosenWeekDay;

        String payload = "STW 21 6 0"+_space+curTime+_space+curDate+_space+alarmTime+_space+alarmMask+_space+alarmDateWeekSel+_space+weekday+"\r\n";

        return payload;
    }

    private String toLittleEndianWithSpaces(int value){
        String result = "";

        int msb = value % 100;
        msb = 16*(msb/10) + msb%10;

        int sb2 = (value/100)%100;
        sb2 = 16*(sb2/10) + sb2%10;

        int sb3 = (value/10000)%100;
        sb3 = 16*(sb3/10) + sb3%10;

        int lsb = (value/1000000)%100;
        lsb = 16*(lsb/10) + lsb%10;

        result = result+msb+_space+sb2+_space+sb3+_space+lsb;

        return result;
    }
}
