package info.androidhive.actionbar;

/**
 * Created by hjacker on 08.06.15.
 */
public class PersonalData {

    private static String emergencyNumber;
    private static String emergencyMsg;

    private static String rejectMsg;


    public static String getRejectMsg() {
        return rejectMsg;
    }

    public static void setRejectMsg(String rejectMsg) {
        PersonalData.rejectMsg = rejectMsg;
    }

    public static String getEmergencyMsg() {
        return emergencyMsg;
    }

    public static void setEmergencyMsg(String emergencyMsg) {
        PersonalData.emergencyMsg = emergencyMsg;
    }

    public static String getEmergencyNumber() {
        return emergencyNumber;
    }

    public static void setEmergencyNumber(String emergencyNumber) {
        PersonalData.emergencyNumber = emergencyNumber;
    }
}
