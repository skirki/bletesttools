package info.androidhive.actionbar;

import android.os.Bundle;
import android.widget.TextView;

public class AntiLostActivity_6 extends AbstractTrackingActivity{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action6);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        doUnbindService();
    }

    @Override
    void onRssiRefresh(Integer rssi, Double avgRssi) {
        TextView rssi_field = (TextView) findViewById(R.id.rssi_value);
        rssi_field.setText(rssi.toString());

        TextView avg_field = (TextView) findViewById(R.id.avg_field);
        avg_field.setText(avgRssi.toString());
    }

    @Override
    void onBleServiceConnected() {

    }

    @Override
    void onBleServiceDisconnected() {

    }

    @Override
    void onReturnFromActivity() {

    }

}
