package info.androidhive.communication;

/**
 * Created by hjacker on 14.01.15.
 */
public class AppMessages {

    //Message code string
    public static final String APP_COMMAND_MESSAGE = "info.androidhive.communication.cmdMessage";
    //Request Commands Messages:
    public static final String BLE_SCAN = "info.androidhive.command.startScan";
    public static final String SCAN_END = "info.androidhive.ble.action.scan_end";
    public static final String SCAN_START = "info.androidhive.ble.action.scan_start";

    public static final String CONNECTED_TO = "info.androidhive.ble.action.connected_to";

    //Application action
    public static final String APP_ACTION = "info.androidhive.appAction";

    //Results codes:
    public static final Integer BLUETOOTH_NOT_ENABLED = -1;
    public static final Integer BLUETOOTH_ENABLED = 0;

    public static final String CONNECTION_TO_NOT_VALID_DEVICE = "info.androidhive.communication.connectionToNotValidDevice";
    public static final String SUCCESSFULLY_CONNECTED_TO_DEVICE = "info.androidhive.communication.successfullyConnectedToDevice";
    public static final String BLE_DEVICE_DISCONNECTED = "info.androidhive.communication.bleDeviceDisconnected";
    public static final String BLE_RSSI_UPDATED = "info.androidhive.communication.bleRssiUpdated";
    public static final String ACC_PEDOMETER_TARGET_UPDATED = "info.androidhive.communication.accPedometerTargetsUpdated";
    public static final String FLASH_MEMORY_UPDATED  = "info.androidhive.communication.flashMemoryUpdated";

    public static final String ACTION_TAP_RECEIVED = "info.androidhive.communication.actionTapRecived";
    public static final String ACTION_TRIPLE_TAP_RECEIVED = "info.androidhive.communication.actionTripleTapReceived";
    public static final String ACTION_ACC_DRIVER_READ = "info.androidhive.communication.accDriverRead";
    public static final String ACTION_ACC_DATA_READ = "info.androidhive.communication.accDataRead";
    public static final String ACTION_BLE_DRIVER_READ = "info.androidhive.communication.bleDriverRead";
    public static final String ACTION_RTC_DRIVER_READ = "info.androidhive.communication.rtcDriverRead";
    public static final String ACTION_RTC_DATA_READ = "info.androidhive.communication.rtcDataRead";
    public static final String ACTION_FLASH_DATA_READ = "info.androidhive.communication.flashDataRead";
}
