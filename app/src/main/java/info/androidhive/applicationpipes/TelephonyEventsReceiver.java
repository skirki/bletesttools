package info.androidhive.applicationpipes;


import android.content.Context;

import info.androidhive.applicationpipes.result.SmsEventResult;
import info.androidhive.applicationpipes.result.TelephonyEventResult;
import info.androidhive.applicationpipes.wrappers.SmsEventWrapper;
import info.androidhive.applicationpipes.wrappers.TelephonyWrapper;

public interface TelephonyEventsReceiver {

    public SmsEventResult smsEventReceive(SmsEventWrapper sms);

    public TelephonyEventResult incomingCallEventReceive(TelephonyWrapper telephone);

    public Context getContext();

}
